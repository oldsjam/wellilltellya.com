import logging
import json
from pprint import pprint

message = "well hello there"

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def get(event, context):
  response = {
    "statusCode": 200,
    "body": message
  }

  logger.info('get function called')
  logger.info(json.dumps(event))
  return response

def update(event, context):
  try:
    logger.info('update function called with event:')
    logger.info(event["queryStringParameters"]['message'])

    response = {
      "statusCode": 200,
      "body": event["queryStringParameters"]['message']
    }

    return response
  except KeyError as e: 
    logger.error('Invalid request')

