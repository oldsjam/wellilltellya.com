# Examples

## Making a valid request
    curl -s https://wellilltellya.com/update\?message\=mymessage

## Full deploy (changes to serverless.yml)
    sls deploy

## Quickly deploy function only
    sls deploy -f <function>

## Getting function logs locally
    sls logs -f <function>

## Local function invocation
    sls invoke local -f <function>

### with data
    sls invoke local -f <function> --data "hello world"
    sls invoke local -f <function> --data '{"a":"bar"}'

### with data from standard input
    node dataGenerator.js | sls invoke local -f <function>

### with data passing
    sls invoke local -f <function> --path entry_data.json

This example will pass the json data in the lib/data.json file (relative to the root of the service) while invoking the specified/deployed function.
